<?php
namespace ErickRocha\CaseStudy\Api;

use ErickRocha\CaseStudy\Model\KeychainInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface KeychainRepositoryInterface 
{
    public function save(KeychainInterface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(KeychainInterface $page);

    public function deleteById($id);
}
