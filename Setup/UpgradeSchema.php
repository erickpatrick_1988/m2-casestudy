<?php
namespace ErickRocha\CaseStudy\Setup;

class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{
    public function upgrade(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $upgrader = $setup;
        $upgrader->startSetup();

        $tableName = $upgrader->getTable('erickrocha_casestudy_keychain');

        if (version_compare($context->getVersion(), '1.2.1', '<')) {
            if ($upgrader->getConnection()->isTableExists($tableName)) {
                $upgrader->getConnection()->changeColumn(
                    $tableName,
                    'usage',
                    'usages',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'length' => 11, 
                        'nullable' => false, 
                        'default' => '1', 
                        'comment' => 'Number of Usages',
                    ]
                );
            }
        }

        
        //END   table setup
        $upgrader->endSetup();
    }
}
