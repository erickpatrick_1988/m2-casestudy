<?php
namespace ErickRocha\CaseStudy\Controller\Adminhtml\Index;
class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;        
        return parent::__construct($context);
    }
    
    public function execute()
    {
        $page = $this->resultPageFactory->create();  

        // basic page data
        $page->setActiveMenu('ErickRocha_CaseStudy::keychains_menu');
        $page->getConfig()->getTitle()->prepend(__('Keychains'));

        // breadcrumbs
        $page->addBreadcrumb(__('Keychains'), __('Keychains'));

        return $page;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('ErickRocha_CaseStudy::keychains_menu');
    }   
}
