<?php

namespace ErickRocha\CaseStudy\Block;

use Magento\Framework\View\Element\Template;

class Main extends Template
{
    private $key;
    private $keychainFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \ErickRocha\CaseStudy\Model\KeychainFactory $keychainFactory
    )
    {
        $this->keychainFactory = $keychainFactory;
        parent::__construct($context);
    }

    public function _prepareLayout()
    {
        $this->key = $this->getRequest()->getParam('key');
    }

    /**
     * Gets key sent through the url and validates it before showing on the
     * screen
     */
    public function getKey()
    {
        $keychain = $this->keychainFactory->create();
        return $keychain->validate($this->key);
    }

    public function hasKey()
    {
        return $this->key !== null;
    }

    /**
     * Retrieve form action url and set "secure" param to avoid confirm
     * message when we submit form from secure page to unsecure
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        return $this->getUrl('casestudy/keychain/test', ['_secure' => true]);
    }
}
