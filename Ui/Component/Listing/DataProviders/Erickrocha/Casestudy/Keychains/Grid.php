<?php
namespace ErickRocha\CaseStudy\Ui\Component\Listing\DataProviders\Erickrocha\Casestudy\Keychains;

class Grid extends \Magento\Ui\DataProvider\AbstractDataProvider
{    
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \ErickRocha\CaseStudy\Model\ResourceModel\Keychain\CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }
}
