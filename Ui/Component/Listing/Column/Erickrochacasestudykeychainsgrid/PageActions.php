<?php
namespace ErickRocha\CaseStudy\Ui\Component\Listing\Column\Erickrochacasestudykeychainsgrid;

class PageActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource["data"]["items"])) {
            foreach ($dataSource["data"]["items"] as & $item) {
                $name = $this->getData("name");
                $id = "X";
                if(isset($item["erickrocha_casestudy_keychain_id"]))
                {
                    $id = $item["erickrocha_casestudy_keychain_id"];
                }
                $item[$name]["view"] = [
                    "href"=>$this->getContext()->getUrl(
                        "adminhtml/erickrocha_casestudy_keychains_grid/viewlog",["id"=>$id]),
                    "label"=>__("Edit")
                ];
            }
        }

        return $dataSource;
    }    
    
}
