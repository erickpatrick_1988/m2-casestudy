<?php
namespace ErickRocha\CaseStudy\Model;
class Keychain extends \Magento\Framework\Model\AbstractModel implements KeychainInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'erickrocha_casestudy_keychain';
    const FIRST_USAGE_KEYCHAIN_VALUE = 1;

    protected function _construct()
    {
        $this->_init('ErickRocha\CaseStudy\Model\ResourceModel\Keychain');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    /**
     * @param string $key
     *
     * @throws \Exception
     */
    public function validate(string $key)
    {
        $feedback = '';

        $key = $this->filterKey($key);
        
        try {
            $this->validateKeyLength($key);
            //$this->validateKeyFormat($key);
            $this->saveKeyUsage($key);
        } catch (\Exception $exception) {
            $feedback = $exception->getMessage();
        }
    
        return $feedback !== '' ? $feedback : $key;
    }

    /*
     * @param string $key
     *
     * @return string
     */
    private function filterKey(string $key)
    {
        return preg_replace('/[^-a-zA-Z0-9\ ]/', '', $key);
    }

    /**
     * @param string $key
     *
     * @throws \Exception
     */
    private function validateKeyLength(string $key)
    {
        $keyLength = strlen($key);

        if ($keyLength === 0) {
            throw new \Exception('Invalid key: no key sent');
        }

        if ($keyLength < 3) {
            throw new \Exception('Invalid key: length is too short');
        }

        if ($keyLength > 30) {
            throw new \Exception('Invalid key: length is too long');
        }
    }

    /**
     * @param string $key
     *
     * @throws \Exception
     */
    private function validateKeyFormat(string $key)
    {
        // check key for bad words?
        // if invalid
            // throw new \Exception('Invalid key: bad words not allowed');
    }

    /**
     * @param string $key
     */
    private function saveKeyUsage(string $key)
    {
        // look for key in custom table
        $keychainCollection = $this->getCollection()
            ->addFieldToFilter('key', ['eq' => $key]);

        $total = count($keychainCollection);

        if ($total === 1) {
            $this->updateKeyUsage($keychainCollection);
        }

        if ($total === 0) {
            // save to table
            $this->setData('key', $key);
            $this->setData('usage', self::FIRST_USAGE_KEYCHAIN_VALUE);
            $this->save();
        }
    }

    /**
     * @param
     */
    private function updateKeyUsage(\ErickRocha\CaseStudy\Model\ResourceModel\Keychain\Collection $keychainCollection)
    {
        foreach ($keychainCollection as $keychain) {
            $updatedUsage = $keychain->getUsage() + 1;

            $keychain->setData('usage', $updatedUsage);
            $keychain->save();
        }
    }

}
