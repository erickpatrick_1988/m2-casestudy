<?php
namespace ErickRocha\CaseStudy\Model\ResourceModel\Keychain;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('ErickRocha\CaseStudy\Model\Keychain','ErickRocha\CaseStudy\Model\ResourceModel\Keychain');
    }
}
