<?php
namespace ErickRocha\CaseStudy\Model\ResourceModel;
class Keychain extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('erickrocha_casestudy_keychain','erickrocha_casestudy_keychain_id');
    }
}
